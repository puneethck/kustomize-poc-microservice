# Introduction

In modern application development, it is crucial to externalize configuration to enable flexibility and maintainability. When working with Spring Boot applications deployed in Kubernetes, managing configurations becomes even more important. Fortunately, Kustomize, a native configuration management tool for Kubernetes, provides an elegant solution. In this below guide, we will explore how to leverage Kustomize to externalize configuration in a Spring Boot application. We will see how Kustomize overlays can be used to customize and manage configuration properties, including the use of ConfigMaps and Secrets. By the end of this post, you'll have a clear understanding of how to streamline your Spring Boot application's configuration management using Kustomize in a Kubernetes environment. Let's get started!

# Externalizing Configuration with Kustomize

Kustomize is a powerful tool for managing Kubernetes configurations, allowing you to customize and extend base configurations without modifying the original files. It provides a declarative approach to configuration management, making it an excellent choice for externalizing configuration in a Spring Boot application deployed in Kubernetes.

## Introduction to Kustomize Overlays:

Kustomize uses overlays to modify or extend base configurations.

Overlays allow you to specify customizations for different environments, such as development, staging, or production.

Each overlay can contain a set of patches or additional resources specific to that environment.

## Creating Kustomization Files:

To start using Kustomize, create a <b>kustomization.yaml</b> file in the root of your project.

This file serves as the entry point and describes the base and overlay configurations.

Define the base configuration, which includes your original Kubernetes manifests, in the resources section of the kustomization.yaml file.

## Creating Overlays for Configuration:

In the same <b>kustomization.yaml</b> file, define overlays specific to configuration customization.

Use the configMapGenerator section to generate ConfigMaps from property files.

Specify the path to your property file and the key-value pairs to be included in the generated ConfigMap.

## Overriding Spring Boot Configuration Properties:

Once you have created the ConfigMap using the overlay, you can override the Spring Boot configuration properties.

Update your Spring Boot deployment manifests to inject the ConfigMap values as environment variables.

Modify the env section of the deployment YAML file to reference the ConfigMap keys and values.

## Applying the Kustomize Overlays:

Apply the Kustomize overlays to your base configuration using the <b>kubectl apply -k</b> command.

This command recursively applies all the overlays and generates the final configuration for your Spring Boot application.

Verify that the updated configuration, including the overridden properties, is applied to your running application.

## Managing ConfigMaps with Kustomize:

Kustomize provides the ability to manage ConfigMaps separately from the application code.

You can maintain different ConfigMaps for different environments and easily switch between them during deployments.

Update the ConfigMap values in the overlay files, and Kustomize will handle the rest.

By leveraging Kustomize overlays and ConfigMaps, you can externalize and manage your Spring Boot application's configuration seamlessly in a Kubernetes environment. Kustomize simplifies the process of customizing and applying configurations, while ConfigMaps provide a flexible and dynamic way to inject properties into your application. With these tools in hand, you can ensure smooth and efficient configuration management for your Spring Boot applications.


![Example Image](./image/kustomize.jpeg)


# Main features of this solution

* This solution externalizes all the spring boot application properties into separate folder inside same git repo. 
* This solution uses native kubernetes configmap and injects application properties during runtime as an env properties.
  application code and the properties are loosely coupled, in other words, solution doesn't requires to rebuild the binaries 
  for any updates required for application properties.
* This solution uses kustomize(K8s native configuration management) features. 
* This solution uses kustomize overlays to manage different environment properties.
* This solution can be extended to K8s secrets and patch updates 

# Pre requisite installation
* Java 17
* Gradle
* kubernetes docker for desktop/minikube
* Kustomize

# Tech
* Java 17
* Springboot
* Gradle
* kubernetes/Docker for Desktop
* Kustomize

# Build

```
./gradlew build

docker build -t config-api:v1 .
```

# Run
```
This command produces K8s deployment,service and configmap definition files and applies to K8s cluster

kustomize build deployment/base | kubectl apply -f -
```


```
If you want to see the K8s definition file before applying to cluster , perform command below

kustomize build deployment/base

apiVersion: v1
data:
  api.data.env: base
  server.port: "8080"
kind: ConfigMap
metadata:
  name: config-api-autogenerated-cm-env
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: bvnk-payment
  name: config-api
spec:
  ports:
  - name: http
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: bvnk-payment
  type: LoadBalancer
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: bvnk-payment
    service: config-api
  name: config-api
spec:
  progressDeadlineSeconds: 90
  replicas: 1
  selector:
    matchLabels:
      app: bvnk-payment
      service: config-api
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: bvnk-payment
        service: config-api
    spec:
      containers:
      - envFrom:
        - configMapRef:
            name: config-api-autogenerated-cm-env
        image: config-api:v1
        imagePullPolicy: IfNotPresent
        name: config-api
        ports:
        - containerPort: 8080
          name: http
        resources:
          limits:
            cpu: 300m
            memory: 600Mi
          requests:
            cpu: 200m
            memory: 400Mi
      restartPolicy: Always

```

# Test

```
http://localhost:8080/api/config/env
Since you have applied configuration from deployment/base (kustomize build deployment/base) , you will see response as "base"
```

# Run 2 

```
kustomize build deployment/overlays/dev/base | kubectl apply -f -

By applying above configuration, only configmap will be updated, no updates to K8s service/deployment

you need to restart the pod to get the latest configmap applied to pod

kubectl scale deployment/config-api --replicas=0
 
kubectl scale deployment/config-api --replicas=1
```

# Test 2

```
http://localhost:8080/api/config/env

Since you have applied configuration from deployment/overlays/dev/base (kustomize build deployment/overlays/dev/base) , you will see response as "dev"
```


# Commands Together
```
./gradlew build

docker build -t config-api:v1 .

kustomize build deployment/base | kubectl apply -f -

http://localhost:8080/api/config/env

kustomize build deployment/overlays/dev/base | kubectl apply -f -

kubectl scale deployment/config-api --replicas=0
 
kubectl scale deployment/config-api --replicas=1

http://localhost:8080/api/config/env

kustomize build deployment/overlays/sandbox/base | kubectl apply -f -

kubectl scale deployment/config-api --replicas=0
 
kubectl scale deployment/config-api --replicas=1

http://localhost:8080/api/config/env

kustomize build deployment/overlays/staging/base | kubectl apply -f -

kubectl scale deployment/config-api --replicas=0
 
kubectl scale deployment/config-api --replicas=1

http://localhost:8080/api/config/env

other endpoints available for testing

curl http://localhost:8080/api/config/appName

curl http://localhost:8080/api/config/actuator/health
curl http://localhost:8080/api/config/actuator/info
curl http://localhost:8080/api/config/actuator/metrics
curl http://localhost:8080/api/config/actuator/prometheus
curl http://localhost:8080/api/config/actuator/beans
curl http://localhost:8080/api/config/actuator/conditions

```

```agsl
kustomize build deployment/base | kubectl apply -f -
kustomize build deployment/overlays/dev/base | kubectl apply -f -
kustomize build deployment/overlays/sandbox/base | kubectl apply -f -
kustomize build deployment/overlays/staging/base | kubectl apply -f -
kustomize build deployment/overlays/prod/base | kubectl apply -f -
kubectl scale deployment/config-api --replicas=0
```

### Additional Links
[Kustomize](https://kustomize.io/)
