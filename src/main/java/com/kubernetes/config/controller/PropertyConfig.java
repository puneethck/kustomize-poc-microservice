package com.kubernetes.config.controller;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "api.data")
@Getter
@Setter
public class PropertyConfig {
  private String env;

  private String appName;

  private String user;

  private String password;
}
