package com.kubernetes.config.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ConfigController {

  private PropertyConfig propertyConfig;

  public ConfigController(PropertyConfig propertyConfig) {
    this.propertyConfig = propertyConfig;
  }

  @GetMapping("/env")
  public String hello(){
    return propertyConfig.getEnv();
  }


  @GetMapping("/appName")
  public String getAppName(){
    return propertyConfig.getAppName();
  }

  @GetMapping("/user")
  public String getUsername(){
    return propertyConfig.getUser();
  }

  @GetMapping("/password")
  public String getPassword(){
    return propertyConfig.getPassword();
  }

}
